﻿using System;
using System.Collections.Generic;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Disco> discos = new List<Disco>();
            discos.Add(new Dvd(new CapaSimple(5), 4.7));
            discos.Add(new Dvd(new CapaDoble(8), 8.5));
            discos.Add(new BlueRay(new CapaDoble(20),25));
            discos.Add(new BlueRay(new CapaDoble(40), 50));

            foreach (var disco in discos)
            {
                disco.Imprimir();
            }
        }


    }
}
