﻿using System;
namespace TP3
{
    public abstract class Capa
    {
        public Double Precio { get; set; }

        public object Nombre { get; set; }

        public Capa(String nombre, Double precio)
        {
            this.Nombre = nombre;
            this.Precio = precio;
        }
    }
}
