﻿using System;
namespace TP3
{
    public abstract class Disco
    {
        public String Nombre { get; }

        public Capa TipoCapa { get;}

        public Double Capacidad { get;}

        public Disco(String nombre, Capa tipoCapa, Double capacidad)
        {
            this.Nombre = nombre;
            this.TipoCapa = tipoCapa;
            this.Capacidad = capacidad;
        }

        public void Imprimir()
        {
            Console.WriteLine("Disco: " + Nombre
                + ", Capa: " + TipoCapa.Nombre
                + ", Precio: " + TipoCapa.Precio
                + ", Capacidad: " + Capacidad);
        }
    }
}
